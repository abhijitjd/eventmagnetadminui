import React, { Component } from 'react';
import clsx from 'clsx';
import PersonIcon from '@material-ui/icons/Person';
import '../../components/common/Common.css';
import Popup from "reactjs-popup";
import {
    Button, TextField, Grid, withStyles, AppBar, Toolbar, CssBaseline,
    Typography, useMediaQuery
} from '@material-ui/core';
import '../../components/common/CommonModal.css';
import { withRouter } from 'react-router-dom';
import Loader from '../../components/loader/Loader';
import api from '../../components/common/APIValues';
import FarmarLogo from '../../components/common/farmar-logo.jpeg';

const useStyles = theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: 0,
        width: `calc(100% - ${0}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    alignment: {
        flexGrow: 1,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    topMargin: {
        marginTop: 40,
    },
    backColor: {
        backgroundColor: '#347f58'
    }
});

const withMediaQuery = (...args) => Component => props => {
    const mediaQuery = useMediaQuery(...args);    
    return <Component mediaQuery={mediaQuery} {...props} />;
};

const validateForm = (errors) => {
    let valid = true;
    Object.keys(errors).map(function (e) {
        if (errors[e].length > 0) {
            valid = false;
        }        
    });
    return valid;
}

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false, email: null, password: null, errorMessage: null, loading: false,
            errors: {
                email: '',
                password: '',
            }
        };
        this.openLogin = this.openLogin.bind(this);
        this.closeLogin = this.closeLogin.bind(this);
        this.popupOnClose = this.popupOnClose.bind(this);
    }

    openLogin() {
        this.setState({ open: true });
    }
    closeLogin() {
        let errors = this.state.errors;
        errors.email = errors.password = '';
        this.setState({ errors, open: false, errorMessage: null });
    }
    popupOnClose() { }

    loginToDashboard = (event) => {
        event.preventDefault();
        if (validateForm(this.state.errors) && this.state.email && this.state.password) {
            this.setState({ loading: true });
            sessionStorage.setItem('loggedInUser', this.state.email)
            const { history } = this.props;
            if (history) history.push('/home/categories');
            //this.validateUser(this.state.email, this.state.password);
        } else {
            let errors = this.state.errors;
            if (!this.state.email) {
                errors.email = 'Email is required';
            }
            if (!this.state.password) {
                errors.password = 'Password is required';
            }
            this.setState({ errors, errorMessage: null });
        }
    }

    validateUser(email, password) {
        var values = { Email: email, Password: password };
        let partialUrl = api.URL;
        fetch(partialUrl + 'Home/ValidateUser', {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(values),
            headers: { 'Content-Type': 'application/json' }
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson) {                                        
                    sessionStorage.setItem('loggedInUser', this.state.email)
                    sessionStorage.setItem('loggedInUserRole', responseJson)
                    const { history } = this.props;
                    if (history) history.push('/Dashboard');
                }
                else {
                    this.setState({
                        errorMessage: 'Invalid email or password',
                        loading: false, email: null, password: null,
                    });
                }
            })
    }

    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;

        switch (name) {
            case 'email':
                this.state.email = value;
                errors.email = value.length <= 0
                ? 'Email is required' : !validEmailRegex.test(value) ? 'Email is not valid' : '';
                break;
            case 'password':
                this.state.password = value;
                errors.password = value.length <= 0
                    ? 'Password is required' : '';
                break;
            default:
                break;
        }
        this.setState({ errors, [name]: value });
    }

    render() {
        const { classes, mediaQuery } = this.props;
        const title = 'EVENT MAGNET';
        const col6 = mediaQuery ? 6 : 12;
        const col3 = mediaQuery ? 6 : 3;

        return (
            <div>
                <div className={classes.root}>
                    <CssBaseline />
                    <AppBar style={{ backgroundColor: 'white' }}
                        position="fixed"
                        className={clsx(classes.appBar, {
                            [classes.appBarShift]: this.state.open,
                        })}>
                        <Toolbar className="header-color">
                            <Typography variant="h6" className={classes.alignment}>
                                <span className="header-font">                                    
                                    { title }
                                </span>
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>

                {this.state.loading ? (
                    <Loader />
                ) : (
                        <div>
                            <div className="home-body">     
                            </div>

                            <Grid container spacing={0}>
                                { mediaQuery ?
                                (<Grid item xs={col6}>                                    
                                    <Grid container spacing={0}>
                                        <Grid item xs={col3}>                                            
                                        </Grid>
                                        <Grid item xs={6}>
                                            <img src={FarmarLogo} alt="Logo" />
                                        </Grid>
                                        <Grid item xs={col3}>                                            
                                        </Grid>
                                    </Grid>
                                </Grid>) : (
                                <Grid item xs={col6} style={{ textAlign: 'center' }}>
                                    <img src={FarmarLogo} alt="Logo" />
                                </Grid>
                                )}
                                <Grid item xs={col6} style={{ marginTop: 32, marginLeft: mediaQuery? 0 : 20, marginRight: mediaQuery? 0 : 20 }}>
                                    <Grid container spacing={0}>                                        
                                        <Grid item xs={col6}>
                                            <TextField required="true" name="email" fullWidth id="txtEmail" label="Email"
                                                onChange={this.handleChange} noValidate />
                                            {this.state.errors.email.length > 0 &&
                                                <span className='error'>{this.state.errors.email}</span>}
                                        </Grid>
                                        <Grid item xs={col6}></Grid>
                                        <Grid item xs={col6}>
                                            <TextField fullWidth required="true" name="password" id="txtPassword" label="Password" type="password"
                                                onChange={this.handleChange} noValidate />
                                            {this.state.errors.password.length > 0 &&
                                                <span className='error'>{this.state.errors.password}</span>}
                                        </Grid>
                                        <Grid item xs={col6}></Grid>
                                        <Grid item xs={col6}>
                                            <div className="marginTop">
                                                <Button fullWidth style={{ backgroundColor: '#2b494b' }} variant="contained"
                                                    color="primary" onClick={this.loginToDashboard}>LOGIN</Button>
                                            </div>
                                        </Grid>
                                        <Grid item xs={col6}></Grid>
                                        {
                                            this.state.errorMessage &&
                                            <Grid item xs={col6} className='error-main'>{this.state.errorMessage}</Grid>
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                        </div>
                    )}
            </div>
        );
    }
}

export default withRouter(withStyles(useStyles)(withMediaQuery('(min-width:600px)')(Home)))